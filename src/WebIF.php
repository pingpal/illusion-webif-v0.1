<?php

namespace Illusion\WebIF;

use Illusion\Core\Launch;

use Illusion\Core\Util;

use Exception;

/**
 * # alias webif
 * # access webif
 * # context cli
 */
class WebIF {

    /**
     * # direct webifsimple
     */
    static function simple($args, $in, $out, $ctx) {

        $port = t($args, 'port/positive?digit', 1337);
        $host = t($args, 'host/text', '0.0.0.0');

        $server = new Server();

        $server->setRequestHandler(new DefaultImpl());

        $server->on('failure', function ($exception) use ($ctx, $port) {

            $ctx->out()->put("<red>Could not listen to port</red> <bold>$port</bold>");
        });

        $server->on('success', function () use ($ctx, $port, $host) {

            $ctx->out()->put("<green>Listening on</green> <bold>$host</bold>:<bold>$port</bold>");
        });

        $server->startHttpServer($port, $host);
    }

    /**
     * # direct webif
     */
    static function _run($args, $in, $out, $ctx) {

        try {

            $that = new self;

            $loop = $that->server->getEventLoop();

            EnvArgs::instance()->set($args);
            EnvArgs::instance()->set('loop', $loop);

            @$args['init'] && l($args['init'], $args, $ctx);

            Launch::instance()->reality();

            $that->run(@$args['port'], @$args['host'], @$args['www'], function ($host, $port) use ($out) {

                $out->put("<green>Listening on port</green> <bold>$host:$port</bold>");

            }, function ($cb) use ($ctx) { $ctx->onProcessExit($cb); });

        } catch (Exception $e) {

            $out->end('<red>' . $e->getMessage() . '</red>');
        }
    }

    protected $server;

    function __construct() {

        $this->server = new Server();

        $this->server->setRequestHandler(new DefaultImpl());
    }

    function run($port = null, $host = null, $www = null, $success = null, $leaving = null) {

        $path = t($www, 'text');
        $port = t($port, 'positive?digit', 1337);
        $host = t($host, 'text', '0.0.0.0');
        $pass = rand(2000, 65000);
        $back = '127.0.0.1';

        if (!$path) {

            $pass = $port;
            $back = $host;

        } else {

            if (`lsof -i :$port`) {

                throw new Exception("Port seems to be in use $port");
            }

            $dir = realpath(__DIR__ . '/..');
            $user = get_current_user();
            $name = 'localhost';
            $r = Util::key(10);

            $this->replace("$dir/nginx.conf", "/[^\s]+;#user/", "$user;#user");
            $this->replace("$dir/nginx.conf", "/[^\s]+;#name/", "$name;#name");
            $this->replace("$dir/nginx.conf", "/-[^-]+;#rfix/", "-$r;#rfix");
            $this->replace("$dir/nginx.conf", "/\.[^\.]+\.log;#rlog/", ".$r.log;#rlog");
            $this->replace("$dir/nginx.conf", "/[^\s]+;#path/", "$path;#path");
            $this->replace("$dir/nginx.conf", "/[\w.]+:\d+;#both/", "$host:$port;#both");
            $this->replace("$dir/nginx.conf", "/\d+;#port/", "$port;#port");
            $this->replace("$dir/nginx.conf", "/\d+;#pass/", "$pass;#pass");

            // Tries 5 times if address in use. Should be able to conf to try once.
            exec("nginx -c $dir/nginx.conf -g \"pid $dir/nginx.pid;\" 2>&1", $print, $error);

            if ($error) {

                if ((exec("nginx -v 2>&1", $null, $error)||1) && $error) {

                    throw new Exception("Nginx was not found");
                }

                throw new Exception("Could not listen to port $port");
            }

            $pid = +Util::file("$dir/nginx.pid");

            $leaving(function () use ($pid) {

                `kill $pid`;
            });
        }

        $this->server->on('failure', function ($exception) use ($pass) {

            throw new Exception("Could not listen to random port $pass");
        });

        $this->server->on('success', function () use ($success, $host, $port) {

            $success && $success($host, $port);
        });

        $this->server->startHttpServer($pass, $back);
    }

    function replace($file, $search, $replace) {

        $content = Util::file($file);
        $replaced = preg_replace($search, $replace, $content);

        if ($content != $replaced) {

            Util::write($file, $replaced);
        }
    }
}
