<?php

namespace Illusion\WebIF;

class EnvArgs {

    static protected $instance;

    static function instance() {

        return self::$instance ?: self::$instance = new self;
    }

    protected $args = [];

    function get($key = null, $def = null) {

        return $key ? isset($this->args[$key]) ? $this->args[$key] : $def : $this->args;
    }

    function set($key, $val = null) {

        is_array($key) ? $this->args = $key : $this->args[$key] = $val;
    }

    function del($key = null) {

        if ($key) unset($this->args[$key]); else $this->args = [];
    }
}
