<?php

namespace Illusion\WebIF;

use Illusion\Core\PacketHelp;

use Illusion\Core\ReqVars;

use Illusion\Core\RouteHelp;

use Illusion\Core\Router;

use Illusion\Core\Context\CLI;

use Illusion\Core\Context\API;

use Illusion\Core\Context\GUI;

use Illusion\Core\Util;

use Exception;

class DefaultImpl {

    function onRequest($request, $response, $req) {

        $vars   = new ReqVars($req);
        $router = new Router($vars);
        $help   = new RouteHelp($router);

        $write = function ($r, $s) use ($response) {

            if (!$r->headersSent()) {

                $r->headersSent(true);

                $response->writeHead($r->status(), $r->header());
            }

            $response->end($s);
        };

        $packet = $vars->packet();

        if ($packet) {

            $api = new API($vars);

            $api->on('write', $write);

            try {

                $packetHelp = new PacketHelp($api->session(), $packet);

                $packetHelp->login(false);

                $data = $packetHelp->data();

                if (@$data['crud'] && @$data['action'] == 'crud') {

                    $query = $data['crud'];

                    $crud = crud($query);
                    $crud->strict($api);

                    $api->out()->end($crud->end());

                } else {

                    $packetHelp->launch($api);
                }

            } catch (Exception $e) {

                $api->escape($e);
            }

            $api->out()->headersSent() || $api->out()->end('ok');

            return;
        }

        $help->on(function ($context, $action, $args) use ($vars, $write) {

            'api' == $context && $ctx = new API($vars);
            'gui' == $context && $ctx = new GUI($vars);

            if (!@$ctx) {

                throw new Exception('unknown context');
            }

            $ctx->on('write', $write);

            try {

                l($action, $args, $ctx);

            } catch (Exception $e) {


                $ctx->escape($e);
            }

            $ctx->out()->headersSent() || $ctx->out()->end('ok');
        });

        $router->on('/(:?api/)?crud\.eval/(.*)', function ($a, $query) use ($vars, $write) {

            $api = new API($vars);

            $api->on('write', $write);

            try {

                $query = urldecode($query);

                $crud = crud($query);

                $api->out()->end($crud->to());

            } catch (Exception $e) {

                $api->escape($e);
            }

            $api->out()->headersSent() || $api->out()->end('ok');
        });

        $router->on('/crud/(.*)', function ($query) use ($vars, $write) {

            $api = new API($vars);

            $api->on('write', $write);

            try {

                $query = urldecode($query);

                $crud = crud($query);
                $crud->strict($api);

                $api->out()->end($crud->end());

            } catch (Exception $e) {

                $api->escape($e);
            }

            $api->out()->headersSent() || $api->out()->end('ok');
        });

        $router->on('/api/([^/]*)', function ($action) use ($vars, $write) {

            $api = new API($vars);

            $api->on('write', $write);

            try {

                l($action, $api->vars()->args(), $api);

            } catch (Exception $e) {

                $api->escape($e);
            }

            $api->out()->headersSent() || $api->out()->end('ok');
        });

        $router->on(null, null, function () use ($vars, $write) {

            $api = new API($vars);

            $api->on('write', $write);

            $api->out()->failure('bad request');
        });
    }
}
