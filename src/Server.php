<?php

namespace Illusion\WebIF;

use React\Http\Server as ReactHttpServer;

use React\Socket\Server as ReactSocketServer;

use React\EventLoop\Factory as ReactFactory;

use React\EventLoop\StreamSelectLoop as ReactStreamSelectLoop;

use Exception;

class Server {

    protected $handler;

    protected $failure;
    protected $success;

    protected $loop;
    protected $socket;
    protected $http;

    function __construct() {

        $this->loop =     new ReactStreamSelectLoop();
        $this->socket =   new ReactSocketServer($this->loop);
        $this->http =     new ReactHttpServer($this->socket);
    }

    function on($verb, $callback) {

        'failure' == $verb && $this->failure = $callback;
        'success' == $verb && $this->success = $callback;
    }

    function setRequestHandler($handler) {

        $this->handler = $handler;
    }

    function callRequestHandler($request, $response, $reqvars) {

        if ($this->handler) {

            $this->handler->onRequest($request, $response, $reqvars);
        }
    }

    function getEventLoop() {

        return $this->loop;
    }

    function startHttpServer($port = null, $host = null) {

        $port = t($port, 'positive?digit', 1337);
        $host = t($host, 'text', '0.0.0.0');

        $this->http->on('request', function ($request, $response) {

            $req['body']    = null;
            $req['headers'] = $request->getHeaders();
            $req['get']     = $request->getQuery();
            $req['url']     = $request->getPath();
            $req['method']  = $request->getMethod();

            if ($req['method'] == 'POST' && @$req['headers']['Content-Length']) {

                $length = +$req['headers']['Content-Length'];

                $body = '';
                $size = 0;

                $request->on('data', function($data) use ($request, $response, $req, $length, &$body, &$size) {

                    $body .= $data;
                    $size += strlen($data);

                    if ($length <= $size) {

                        $req['body'] = $body;

                        $this->callRequestHandler($request, $response, $req);
                    }
                });

            } else {

                $this->callRequestHandler($request, $response, $req);
            }
        });

        try {

            $this->socket->listen($port, $host);

        } catch (Exception $e) {

            $callback = $this->failure;
            $callback && $callback($e);

            return;
        }

        $callback = $this->success;
        $callback && $callback();

        $this->loop->run();
    }
}
